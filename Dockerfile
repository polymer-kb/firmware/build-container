FROM rust

ARG TOOLCHAIN

RUN rustup default ${TOOLCHAIN:-nightly} && \
	rustup target add thumbv7m-none-eabi && \
	rustup component add rustfmt && \
	rustup component add clippy && \
	cargo install cargo-edit
